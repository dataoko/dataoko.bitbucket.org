
var S = { zoom: 8, lat: 46.197, lng: 14.944, scale: 2500, opacity: 0.12 };

function initialize() {

    var view = window.location.hash.substr(1);
    if (view) {
	S.zoom = 13;
	S.scale = 100000;
	S.opacity = 0.25;
	if (view == "lj") {
	    S.lat = 46.056451;
	    S.lng = 14.50807;
	} else if (view == "mb") {
	    S.lat = 46.546623;
	    S.lng = 15.659485;
	} 
    }
    
    var mapOptions = {
	zoom: S.zoom,
	center: new google.maps.LatLng(S.lat, S.lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
	styles: [
	    {
		stylers: [
		    { lightness: 50 },
		    { saturation: -100 }
		]
	    },{
		featureType: "road",
		elementType: "geometry",
		stylers: [
		    { lightness: 100 },
		    { visibility: "simplified" }
		]
	    },{
		featureType: "road",
		elementType: "labels",
		stylers: [
		    { visibility: "off" }
		]
	    }
	]
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    drawData(DATA1,"#FF0000");
    drawData(DATA2,"#00FF00");
}

function drawData(data,color) {
    for (var i=0; i<data.mapRS.length;i++) {
	drawCircle(new google.maps.LatLng(data.mapRS[i]['lt'],data.mapRS[i]['ln']),parseInt(data.mapRS[i]['g'])/S.scale,color);
    }
}

function drawCircle(center, radius, color) {
    draw_circle = new google.maps.Circle({
        center: center,
        radius: radius,
        strokeColor: "#FFFFFF",
        strokeOpacity: 0,
        strokeWeight: 0,
        fillColor: color,
        fillOpacity: S.opacity,
        map: map
    });
}
