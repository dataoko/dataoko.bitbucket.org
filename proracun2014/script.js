dirmap={"7":"PRIHODKI","4":"ODHODKI","5":"POSOJILA"}
dirmap2={"7":1,"4":-1,"5":-1}
DATA = [null,null];
TREE = [null,null];
PATH = [];
function rootlink(idx){ return "<div class='path'><b><a href='javascript:void(0)' onclick='document.getElementById(\"vis\").innerHTML = rootlink("+idx+
			")+printTree2(TREE,null,[],0,1);STEPS=[]'>Proračun 2013|2014</a></b> &raquo; "
		      }

function draw(data,vis,f_map,valIdx,num) {
    for(var idx=0;idx<num;idx++)
    {
	a = document.getElementById(data+idx).innerHTML;
	var b=a.split("\n");
	c=[]
	for(var i=0;i<b.length;i++){ c.push(b[i].split("\t")) }
	//console.debug(vis+idx);
	DATA[idx] = c.map(f_map);
	TREE[idx] = precalcTree(DATA[idx],valIdx);
    }
    document.getElementById(vis).innerHTML = rootlink(idx)+printTree2(TREE,null,[],0,1);
}

function precalcTree(D,valIdx) {
    T={}
    for(var i=0;i<D.length;i++) {
	var val=parseFloat(D[i][valIdx]);
	if(val){
	    T=addBranch(T,D[i],val,0,valIdx);
	}
    }
    return T;
}

function addBranch(T1,L,val,depth,max) {
    if (depth==max) return [val,null];
    T1[L[depth]] = T1[L[depth]] ? [ T1[L[depth]][0]+val , addBranch(T1[L[depth]][1],L,val,depth+1,max) ] : [ val,addBranch({},L,val,depth+1,max) ];
    return T1;
}

function printTree2(T,name,path,step,idx) {
    PATH=path;
    if(name==path[step-1]){
	sum=0;for(var ii=0;ii<2;ii++){if(T){for(k in T[ii]){sum+=T&&T[ii]&&T[ii][k]?Math.abs(T[ii][k][0]) : 0}}}
	r="</div>";
	for(k in T[idx]){
	    r+="<div class='row'>"
	    for (var ii=0;ii<2;ii++) {
		DD(T[ii][k]);
		if(T[ii] && T[ii][k]) {
		    R = Math.abs(T[ii][k][0]) / sum * 490;
		    var span = '<span style="padding:20px;">'+(T[ii][k][1][1]===null?k:printTreeLeaf(k,T[ii][k][0],sum,ii))+' '+fm(T[ii][k][0],ii)+'</span>';
		    r+='<div class="circ'+ii+'">'+(ii?'':span)+'<a '+(T[ii][k][1][1]===null?'':'href="javascript:void(0)"')+' onclick="expandTo(\''+k+'\')" class="circ '+(T[ii][k][0]<0?'negative':'')+'" style="'+genLeafStyle(k,R,sum,ii)+';vertical-align:middle">&nbsp;</a>'+(ii?span:'')+'</div>';
		} else {
		    r+='<div class="circ'+ii+'">-</div>';
		}
	    }
	    r+="</div>"
	}
	return r+"";
    }else{
	return printTreeLink(path[step],step) + " "+fm(T[idx][path[step]][0])+" &raquo; " + printTree2([T[0][path[step]][1],T[1][path[step]][1]],name,path,step+1,1);
    }
}


function printTreeLink(step,stepN,idx){
    return "<a href='javascript:void(0)' onclick='collapseTo(\""+step+"\","+stepN+","+idx+")'>"+step+"</a>";
}

function printTreeLeaf(step,val,sum,idx){
    return "<a href='javascript:void(0)' onclick='expandTo(\""+step+"\","+idx+")'>"+step+"</a>";
}

function genLeafStyle(step,r,sum,idx){
    return idx!==0?"height:"+(r*2)+"px;width:"+r+";border-radius:0 "+r+"px "+r+"px 0;":
    "height:"+(r*2)+"px;width:"+r+";border-radius:"+r+"px 0 0 "+r+"px;";
}

function collapseTo(step,stepN,idx){
    PATH.splice(stepN+1);
    document.getElementById("vis").innerHTML = rootlink(idx)+printTree2(TREE,step,PATH,0,1);
}
function expandTo(step,idx){
    PATH.push(step);
    document.getElementById("vis").innerHTML = rootlink(idx)+printTree2(TREE,step,PATH,0,1);
}

// utility
function DD(x){console.debug(x);return x}
function fm(x){return Math.abs(x).formatMoney(0,".",",");}
Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};