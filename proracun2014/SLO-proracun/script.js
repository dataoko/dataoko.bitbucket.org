dirmap={"7":"PRIHODKI","4":"ODHODKI","5":"POSOJILA"}
rootlink="<b><a href='javascript:void(0)' onclick='document.getElementById(\"vis\").innerHTML = rootlink+printTree(TREE,null,[],0);STEPS=[]'>Proračun 2013</a></b> &raquo; "

function draw() {
    a = document.getElementById("data").innerHTML;
    var b=a.split("\n");
    c=[]
    for(var i=1;i<b.length;i++){ c.push(b[i].split("\t")) }
    DATA = c.map(function(x){return [x[1],dirmap[(x[2]?x[2][0]:"")],x[3],x[5],x[7],x[8] ]});
    TREE = precalcTree(DATA,5);
    document.getElementById("vis").innerHTML = rootlink+printTree(TREE,null,[],0);
}


// {"bra":[123,{"bl1":[22,{"bl2":100,"cll":200},"cl2":{...}}}

function precalcTree(D,valIdx) {
    T={}
    for(var i=0;i<D.length;i++) {
	var val=parseFloat(D[i][valIdx]);
	if(val){
	    T=addBranch(T,D[i],val,0,valIdx);
	}
    }
    return T;
}

function addBranch(T1,L,val,depth,max) {
    if (depth==max) return [val,null];
    T1[L[depth]] = T1[L[depth]] ? [ T1[L[depth]][0]+val , addBranch(T1[L[depth]][1],L,val,depth+1,max) ] : [ val,addBranch({},L,val,depth+1,max) ];
    return T1;
}

function printTree(T,name,path,step) {
    PATH=path;
    console.debug(T);
    console.debug(name);console.debug(path[step]);
    r="";
    if(name==path[step-1]){
	sum=0;for(k in T){sum+=T[k][0]}
	
	for(k in T){
	    R = T[k][0] / sum * 250;
	    console.debug(T[k][0]);
	    r+='<div style="padding:20px 0;"><span class="circ" style="'+genLeafStyle(k,R,sum)+';vertical-align:middle">&nbsp;</span><span style="padding:20px;">'+printTreeLeaf(k,T[k][0],sum)+' '+fm(T[k][0])+'</span></div>';
	}
	return r+"";
    }else{
	return printTreeLink(path[step],step) + " "+fm(T[path[step]][0])+" &raquo; " + printTree(T[path[step]][1],name,path,step+1);
    }
}


function printTreeLink(step,stepN){
    return "<a href='javascript:void(0)' onclick='collapseTo(\""+step+"\","+stepN+")'>"+step+"</a>";
}

function printTreeLeaf(step,val,sum){
    return "<a href='javascript:void(0)' onclick='expandTo(\""+step+"\")'>"+step+"</a>";
}

function genLeafStyle(step,r,sum){
    return "height:"+(r*2)+";width:"+r+";border-radius:0 "+r+" "+r+" 0;";
}

function collapseTo(step,stepN){
    PATH.splice(stepN+1);
    document.getElementById("vis").innerHTML = rootlink+printTree(TREE,step,PATH,0);
}
function expandTo(step){
    PATH.push(step);
    document.getElementById("vis").innerHTML = rootlink+printTree(TREE,step,PATH,0);
}



// STUFF

function fm(x){return x.formatMoney(0,".",",");}


Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};